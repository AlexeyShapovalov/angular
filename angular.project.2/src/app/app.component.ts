import { Component } from '@angular/core';

import{PanelBottomComponent} from './panel-bottom/panel-bottom.component';
import{PanelContentComponent} from './panel-content/panel-content.component';
import{PanelHeaderComponent} from './panel-header/panel-header.component';
import{PanelFooterComponent} from './panel-footer/panel-footer.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less']
})
export class AppComponent {
  title = 'app';
}
