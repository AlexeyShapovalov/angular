import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { PanelHeaderComponent } from './panel-header/panel-header.component';
import { PanelContentComponent } from './panel-content/panel-content.component';
import { PanelBottomComponent } from './panel-bottom/panel-bottom.component';
import { PanelFooterComponent } from './panel-footer/panel-footer.component';


@NgModule({
  declarations: [
    AppComponent,
    PanelHeaderComponent,
    PanelContentComponent,
    PanelBottomComponent,
    PanelFooterComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
