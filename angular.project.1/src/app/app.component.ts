import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
    
  title = 'Lol';
  description = 'The future of kek cheburek';
  
  list = [
    'https://img4.goodfon.ru/wallpaper/middle/e/76/rossiia-primorskii-krai-leto-vecher-zakat-oblaka-reka.jpg',
    'https://img4.goodfon.ru/wallpaper/middle/a/7f/azur-lane-art-devushka-plate.jpg',
    'https://img4.goodfon.ru/wallpaper/middle/6/13/devochka-chemodan-zheleznaia-doroga-1.jpg',
    'https://img4.goodfon.ru/wallpaper/middle/2/a3/prazdnik-maslenitsa-bliny-ikra.jpg',
    'https://img4.goodfon.ru/wallpaper/middle/a/53/3q-studio-art-wang-zhe-rong-yao-wang-zhao-jun-tong-ren-fente.jpg'
  ];
    
}